#!/bin/sh
############################################################
#                                                          #
#       LAZY ARTIX RUNIT SETUP SCRIPT                      #
#       NO DESKTOP ENVIRONMENT, ONLY BASIC UTILITIES       #
#                                                          #
############################################################

function echo_title() {     echo -ne "\033[1;44;37m${*}\033[0m\n"; }

function splash() {
    local hr
    hr=" **$(printf "%${#1}s" | tr ' ' '*')** "
    echo_title "${hr}"
    echo_title " * $1 * "
    echo_title "${hr}"
    echo
}
## PART 1

printf '\033c'
echo "Welcome to Pixel's artix installer script!"
splash 'Warning! This script assumes you have a separate /home partition !'
sed -i "s/^#ParallelDownloads = 5$/ParallelDownloads = 5/" /etc/pacman.conf
pacman --noconfirm -Sy archlinux-keyring
lsblk


echo "Enter the drive: "
read drive
fdisk $drive

  echo "Enter the root partition: "
  read root
  mkfs.ext4 -L ROOT $root

  echo "Enter the home partition: "
  read home
  mkfs.ext4 -L HOME $home

  echo "Enter EFI partition: "
  read efi
  mkfs.fat -F 32 $efi
  fatlabel $efi BOOT

mount $root /mnt
mkdir -p /mnt/boot/efi
mkdir -p /mnt/home
mount $home /mnt/home
mount $efi /mnt/boot/efi

basestrap /mnt base base-devel linux linux-firmware runit elogind-runit intel-ucode vim
fstabgen -U /mnt >> /mnt/etc/fstab
sed '1,/^##PART 2$/d' setup.sh > /mnt/setup2.sh
chmod +x /mnt/setup2.sh
artix-chroot /mnt ./setup2.sh
exit

##PART 2

printf '\033c'
pacman -S --noconfirm artix-archlinux-support
pacman-key --populate archlinux
sed -i "s/^#ParallelDownloads = 5$/ParallelDownloads = 5/" /etc/pacman.conf

echo "# ARCH
[extra]
Include = /etc/pacman.d/mirrorlist-arch

[community]
Include = /etc/pacman.d/mirrorlist-arch

#[multilib]
#Include = /etc/pacman.d/mirrorlist-arch" >> /etc/pacman.conf
pacman -Sy

ln -sf /usr/share/zoneinfo/Africa/Algiers /etc/localtime
hwclock --systohc

dd if=/dev/zero of=/swapfile bs=2G count=2 status=progress
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
printf "\n\n/swapfile    none    swap  defaults    0 0\n" >> /etc/fstab

echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" > /etc/locale.conf

echo "Hostname: "
read hostname
echo $hostname > /etc/hostname
printf "\n127.0.0.1       localhost\n::1             localhost\n127.0.1.1       $hostname.localdomain $hostname\n">> /etc/hosts
passwd
pacman --noconfirm -S grub efibootmgr os-prober
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
#sed -i 's/GRUB_TIMEOUT=5/GRUB_TIMEOUT=0/g' /etc/default/grub
grub-mkconfig -o /boot/grub/grub.cfg

pacman -S --noconfirm xorg-server xorg-xinit xorg-xkill xorg-xsetroot xorg-xbacklight xorg-xprop xf86-video-intel\
     noto-fonts noto-fonts-emoji noto-fonts-cjk ttf-joypixels ttf-font-awesome ttf-droid ttf-dejavu ttf-roboto \
     sxiv mpv zathura zathura-pdf-mupdf ffmpeg imagemagick \
     fzf man-db man-pages xwallpaper youtube-dl unclutter xclip maim \
     zip unzip unrar p7zip xdotool papirus-icon-theme \
     dosfstools ntfs-3g git zsh pipewire pipewire-pulse \
     vim neovim arc-gtk-theme rsync firefox dash \
     xcompmgr libnotify dunst slock jq valgrind clang llvm bind rust go \
     dhcpcd networkmanager networkmanager-runit network-manager-applet rsync pamixer openssh openssh-runit rsm

rm /bin/sh
ln -s dash /bin/sh
echo "%wheel ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
echo "Enter Username: "
read username
useradd -mG wheel $username
passwd $username
sc_path=/home/$username/setup3.sh
sed '1,/^## PART 3$/d' setup2.sh > $sc_path
chown $username:$username $sc_path
chmod +x $sc_path
su -c $sc_path -s /bin/sh $username
exit

## PART 3

printf '\033c'
cd $HOME

git clone https://aur.archlinux.org/paru-bin.git
cd paru-bin
makepkg -si

echo "SCRIPT IS FINISHED, REBOOT NOW"
echo "run: umount -R /mnt"
echo "Start Network manager and ssh service after reboot with the following commands :"
echo "sudo ln -s /etc/runit/sv/NetworkManager/ /run/runit/service/"
echo "sudo ln -s /etc/runit/sv/sshd/ /run/runit/service/"

exit
